const exec = require('child_process').exec
var version = exec('node --version', {silent:true}).stdout; 
var child = exec('./rsyslog -c config.json', {async:true});
child.stdout.on('data', function(data) {
  /* ... do something with data ... */
});
 
exec('./rsyslog -c config.json', function(code, stdout, stderr) {
  console.log('Exit code:', code);
  console.log('Program output:', stdout);
  console.log('Program stderr:', stderr);
});